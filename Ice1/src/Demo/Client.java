package Demo;

import Demo.generated.DoubleValue;
import Demo.generated.MyEvent;
import Demo.generated.MyWaveform;
import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client {
    protected static final Logger logger = Logger.getLogger("IceZeroc");
    
     /**
     * Configures experiment logger with file and console handlers
     *
     * @param level Log level
     * @throws SecurityException
     * @throws IOException
     */
    protected static void setupLogger(Level level) throws SecurityException, IOException {
        logger.setUseParentHandlers(false);
        logger.setLevel(level);
//        // file handler
//        FileHandler fileHandler = new FileHandler("IceZero.log");
//        fileHandler.setLevel(level);
//        logger.addHandler(fileHandler);
        // console handler
        ConsoleHandler handler = new ConsoleHandler();
        handler.setLevel(level);
        logger.addHandler(handler);
    }


    public static void main(String[] args) {
        try {
            setupLogger(Level.FINE);
        } catch (SecurityException | IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        int status = 0;
        Ice.Communicator ic = null;
        try {
            ic = Ice.Util.initialize(args);
            Ice.ObjectPrx base = ic.stringToProxy(
                    "SimpleStringTransformer:default -p 10000");
            Demo.generated.StringTransformerPrx stringTransformer
                    = Demo.generated.StringTransformerPrxHelper.checkedCast(base);
            if (stringTransformer == null) {
                throw new Error("Invalid proxy");
            }
            logger.fine("PrinterProxy initialised");
            base = ic.stringToProxy(
                    "SimpleCopier:default -p 10000");
            Demo.generated.CopierPrx copier
                    = Demo.generated.CopierPrxHelper.checkedCast(base);
            if (copier == null) {
                throw new Error("Invalid proxy");
            }
            logger.fine("CopierProxy initialised");
            base = ic.stringToProxy(
                    "SimpleScaler:default -p 10000");
            Demo.generated.ScalerPrx scaler
                    = Demo.generated.ScalerPrxHelper.checkedCast(base);
            if (scaler == null) {
                throw new Error("Invalid proxy");
            }
            logger.fine("ScalerProxy initialised");
            
            logger.info("Simple example with built-in types");
            String aString = "Hello World!";
            String rtnString = stringTransformer.capitalise(aString);
            assert(rtnString.equalsIgnoreCase(aString));
            logger.log(Level.INFO, "Return value: {0}", rtnString);
            
            logger.info("Example with user types");
            MyEvent e = new MyEvent();
            e.setId(42);
            MyEvent rtnEvent = copier.copy(e);
            assert(e.getId() == rtnEvent.getId());
            logger.log(Level.INFO, "Returned event with id: {0}", rtnEvent.getId());
            
            logger.info("Class with inheritance and sequences");
            MyWaveform w = new MyWaveform();
            w.setId(22);
            DoubleValue[] values = new DoubleValue[4];
            for (int i = 0; i < 4; i++) {
                DoubleValue dv = new DoubleValue();
                dv.value = i;
                values[i] = dv;
            }
            w.values = values;
            MyWaveform rtnWaveform = scaler.scaleWaveform(w);
            assert (rtnWaveform.getId() == w.getId());
            logger.log(Level.INFO, "Returned waveform with id {0}", rtnWaveform.getId());
            for (int i = 0; i < rtnWaveform.values.length; i++) {
                assert(w.values[i].value * 2 == rtnWaveform.values[i].value);
                logger.log(Level.FINE, "Return value {0} = {1}", new Object[]{i, rtnWaveform.values[i].value});
            }
        } catch (Ice.LocalException e) {
            e.printStackTrace();
            status = 1;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            status = 1;
        }
        if (ic != null) {
            // Clean up
            //
            try {
                ic.destroy();
            } catch (Exception e) {
                System.err.println(e.getMessage());
                status = 1;
            }
        }
        System.exit(status);
    }
}
