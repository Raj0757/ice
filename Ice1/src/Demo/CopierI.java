package Demo;

import Demo.generated.MyEvent;
import Ice.Current;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CopierI extends Demo.generated._CopierDisp {
    protected static final Logger logger = Logger.getLogger("IceZeroc");
    @Override
    public MyEvent copy(MyEvent e, Current __current) {
        logger.log(Level.FINE, "Processign MyEvent with id: {0}", e.getId());
        return e;
    }

}
