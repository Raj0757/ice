module Demo {
    module generated {
        interface StringTransformer {
            string capitalise(string s);
        };
        class DoubleValue {
            double value;
        };
        sequence<DoubleValue> DoubleSeq;

        ["java:getset"] class MyEvent {
            int id;         // 0 ‑ 23
        };
        class MyWaveform extends MyEvent{
            DoubleSeq values;
        };

        interface Scaler {
            MyWaveform scaleWaveform(MyWaveform w);
        };
        interface Copier {
            MyEvent copy(MyEvent w);
        };
    };
};