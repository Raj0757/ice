package Demo;

import Demo.generated.MyWaveform;
import Ice.Current;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ScalerI extends Demo.generated._ScalerDisp {
    protected static final Logger logger = Logger.getLogger("IceZeroc");
    
    @Override
    public MyWaveform scaleWaveform(MyWaveform w, Current __current) {
        logger.log(Level.FINE, "MyWaveform with id: {0}", w.getId());
        for (int i = 0; i < w.values.length; i++) {
            logger.log(Level.FINE, "Value {0} = {1}", new Object[]{i, w.values[i].value});
            w.values[i].value *= 2;
        }
        return w;
    }
}
