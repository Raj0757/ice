package Demo;

import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server {
    protected static final Logger logger = Logger.getLogger("IceZeroc");
    
    /**
     * Configures experiment logger with file and console handlers
     *
     * @param level Log level
     * @throws SecurityException
     * @throws IOException
     */
    protected static void setupLogger(Level level) throws SecurityException, IOException {
        logger.setUseParentHandlers(false);
        logger.setLevel(level);
//        // file handler
//        FileHandler fileHandler = new FileHandler("IceZero.log");
//        fileHandler.setLevel(level);
//        logger.addHandler(fileHandler);
        // console handler
        ConsoleHandler handler = new ConsoleHandler();
        handler.setLevel(level);
        logger.addHandler(handler);
    }

    public static void main(String[] args) {
        try {
            setupLogger(Level.FINE);
        } catch (SecurityException | IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        int status = 0;
        Ice.Communicator ic = null;
        try {
            ic = Ice.Util.initialize(args);
            Ice.ObjectAdapter adapter
                    = ic.createObjectAdapterWithEndpoints(
                            "SimplePrinterAdapter", "default -p 10000");
            Ice.Object object = new StringTransformerI();
            adapter.add(object, ic.stringToIdentity("SimpleStringTransformer"));
            logger.fine("Added adapter SimpleStringTransformer");
            object = new CopierI();
            adapter.add(object, ic.stringToIdentity("SimpleCopier"));
            logger.fine("Added adapter SimpleCopier");
            object = new ScalerI();
            adapter.add(object, ic.stringToIdentity("SimpleScaler"));
            logger.fine("Added adapter SimpleScaler");
            adapter.activate();
            logger.fine("Adapters activated");
            ic.waitForShutdown();
        } catch (Ice.LocalException e) {
            e.printStackTrace();
            status = 1;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            status = 1;
        }
        if (ic != null) {
            // Clean up
            //
            try {
                ic.destroy();
            } catch (Exception e) {
                System.err.println(e.getMessage());
                status = 1;
            }
        }
        System.exit(status);
    }
}
