package Demo;

import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StringTransformerI extends Demo.generated._StringTransformerDisp {
    protected static final Logger logger = Logger.getLogger("IceZeroc");
    
    @Override
    public String capitalise(String s, Ice.Current current) {
        logger.log(Level.FINE, "Capitalising string: {0}", s);
        return s.toUpperCase(Locale.ENGLISH);
    }
}
