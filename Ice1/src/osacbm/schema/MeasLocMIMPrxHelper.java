// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************
//
// Ice version 3.5.1
//
// <auto-generated>
//
// Generated from file `schema.ice'
//
// Warning: do not edit this file.
//
// </auto-generated>
//

package osacbm.schema;

public final class MeasLocMIMPrxHelper extends Ice.ObjectPrxHelperBase implements MeasLocMIMPrx
{
    public static MeasLocMIMPrx checkedCast(Ice.ObjectPrx __obj)
    {
        MeasLocMIMPrx __d = null;
        if(__obj != null)
        {
            if(__obj instanceof MeasLocMIMPrx)
            {
                __d = (MeasLocMIMPrx)__obj;
            }
            else
            {
                if(__obj.ice_isA(ice_staticId()))
                {
                    MeasLocMIMPrxHelper __h = new MeasLocMIMPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static MeasLocMIMPrx checkedCast(Ice.ObjectPrx __obj, java.util.Map<String, String> __ctx)
    {
        MeasLocMIMPrx __d = null;
        if(__obj != null)
        {
            if(__obj instanceof MeasLocMIMPrx)
            {
                __d = (MeasLocMIMPrx)__obj;
            }
            else
            {
                if(__obj.ice_isA(ice_staticId(), __ctx))
                {
                    MeasLocMIMPrxHelper __h = new MeasLocMIMPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static MeasLocMIMPrx checkedCast(Ice.ObjectPrx __obj, String __facet)
    {
        MeasLocMIMPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA(ice_staticId()))
                {
                    MeasLocMIMPrxHelper __h = new MeasLocMIMPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static MeasLocMIMPrx checkedCast(Ice.ObjectPrx __obj, String __facet, java.util.Map<String, String> __ctx)
    {
        MeasLocMIMPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try
            {
                if(__bb.ice_isA(ice_staticId(), __ctx))
                {
                    MeasLocMIMPrxHelper __h = new MeasLocMIMPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            }
            catch(Ice.FacetNotExistException ex)
            {
            }
        }
        return __d;
    }

    public static MeasLocMIMPrx uncheckedCast(Ice.ObjectPrx __obj)
    {
        MeasLocMIMPrx __d = null;
        if(__obj != null)
        {
            if(__obj instanceof MeasLocMIMPrx)
            {
                __d = (MeasLocMIMPrx)__obj;
            }
            else
            {
                MeasLocMIMPrxHelper __h = new MeasLocMIMPrxHelper();
                __h.__copyFrom(__obj);
                __d = __h;
            }
        }
        return __d;
    }

    public static MeasLocMIMPrx uncheckedCast(Ice.ObjectPrx __obj, String __facet)
    {
        MeasLocMIMPrx __d = null;
        if(__obj != null)
        {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            MeasLocMIMPrxHelper __h = new MeasLocMIMPrxHelper();
            __h.__copyFrom(__bb);
            __d = __h;
        }
        return __d;
    }

    public static final String[] __ids =
    {
        "::Ice::Object",
        "::osacbm::schema::MeasLoc",
        "::osacbm::schema::MeasLocMIM"
    };

    public static String ice_staticId()
    {
        return __ids[2];
    }

    protected Ice._ObjectDelM __createDelegateM()
    {
        return new _MeasLocMIMDelM();
    }

    protected Ice._ObjectDelD __createDelegateD()
    {
        return new _MeasLocMIMDelD();
    }

    public static void __write(IceInternal.BasicStream __os, MeasLocMIMPrx v)
    {
        __os.writeProxy(v);
    }

    public static MeasLocMIMPrx __read(IceInternal.BasicStream __is)
    {
        Ice.ObjectPrx proxy = __is.readProxy();
        if(proxy != null)
        {
            MeasLocMIMPrxHelper result = new MeasLocMIMPrxHelper();
            result.__copyFrom(proxy);
            return result;
        }
        return null;
    }

    public static final long serialVersionUID = 0L;
}
